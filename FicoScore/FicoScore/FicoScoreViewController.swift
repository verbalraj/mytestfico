//
//  FicoScoreViewController.swift
//  FicoScore
//
//  Created by Balraj Verma on 8/26/20.
//  Copyright © 2020 Balraj Verma. All rights reserved.
//

import UIKit

public class FicoScoreViewController: UIViewController {
    @IBOutlet weak var ficoScoreLabel: UILabel!
    @IBOutlet weak var apiCallLogTextView: UITextView!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        ficoScoreLabel.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.displayScore()
        }
    }
    
    func displayScore() {
        ficoScoreLabel.isHidden = false
        callAPI()
    }
    
    func changeText(text: String){
        DispatchQueue.main.async {
            self.apiCallLogTextView.text = text
        }
    }
    
    private func callAPI() {
        changeText(text: "Calling api -> apiCallLogTextView")
        var request = URLRequest(url: URL(string: "https://jsonplaceholder.typicode.com/todos/1")!)
        changeText(text: "URL --> https://jsonplaceholder.typicode.com/todos/1")
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                print(json)
                self.changeText(text: "Response True --> \(json)")
            } catch {
                print("error")
                self.changeText(text: "Error occured parsing")
            }
        })
        task.resume()
    }
}


