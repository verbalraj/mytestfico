//
//  FicoViewSmall.swift
//  FicoScore
//
//  Created by Balraj Verma on 8/26/20.
//  Copyright © 2020 Balraj Verma. All rights reserved.
//


import UIKit

public protocol FicoButtonCallBack: class {
    func ficoButtonPressed()
}

public protocol FicoCardbuttonCallBack: class {
    func removeCard(sender: UIButton)
}

public class FicoViewSmall: UIView {
    var view: UIView!
    @IBOutlet weak var cardFirst: UIButton!
    @IBOutlet weak var cardSecond: UIButton!
    open weak var delegate: FicoButtonCallBack?
    open weak var buttonDelegate: FicoCardbuttonCallBack?
    
    var titleText: String?
    var scoreText: String?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: FicoViewSmall.self)
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        
        if let ficoView = nib.instantiate(withOwner: self, options: nil).first as? UIView {
            ficoView.frame = bounds
            ficoView.autoresizingMask = [
                UIView.AutoresizingMask.flexibleWidth,
                UIView.AutoresizingMask.flexibleHeight
            ]
            addSubview(ficoView)
        }
    }
    
    func setUpValues(score:String, title:String) {
        //        self.score.text = score
        //        self.title.text = title
    }
    
    @IBAction func ficoButtonTapped() {
        delegate?.ficoButtonPressed()
    }
    
    @IBAction func removeCardButton(sender: UIButton){
        buttonDelegate?.removeCard(sender: sender)
    }
    
    
}


